package tehran.sematec;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    String name = "ali" ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        name = "javad" ;
    }

    @Override
    protected void onStart() {
        super.onStart();

        name = " Alireza" ;
    }

    @Override
    protected void onResume() {
        super.onResume();
        name ="maryam" ;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
